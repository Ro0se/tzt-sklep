const express = require('express');
const promisify = require('util').promisify;
const mongo = require('mongodb').MongoClient;
const {ObjectId} = require('mongodb');
const assert = require('assert');
var session = require('express-session');
var parser = express.urlencoded({extended: true});

var app = express();
var parser = express.urlencoded({extended: true});
var db;

mongo.connect('mongodb+srv://admin:1jRMe2mC2rdNUCjH@cluster0-uapmc.mongodb.net/test?retryWrites=true&w=majority', function(err, client) {
    assert.equal(null, err);
    db = client.db('shop');
    console.log("Nawiązano połączenie z bazą");
    seed();
});

app.use(session({
    'secret': '343ji43j4n3jn4jk3n'
  }))

app.get('/', async (req, res) => {
    if(req.session.userId != null) {
        var user = await promisify(getUserData)(req.session.userId);
        //var cartCount = user.cart.length
        res.send(`
        <h1>Witaj ` + user.firstName + ` ` + user.lastName + `</h1><br />
        <a href='/cart'>Koszyk</a><br />
        <a href='/getCategories'>Kategorie produktow</a><br />
        <a href='/getAllProducts'>Wszystkie produkty</a><br />
        <a href='/checkoutHistory'>Historia zamówień</a><br />
        <a href='/logout'>Wyloguj</a><br />
        `);
    } else {
        res.send(`
        <h1><a href="/login">Zaloguj się</a></h1>
        <h1><a href="/register">Zarejestruj się</a></h1>
        `);
    }
});

app.get('/login', async (req, res) => {
    res.send(`
    <h1>Test</h1><br />
    <form method="post" action="login">
      Login:<br />
      <input type="text" name="login" /><br />
      Hasło:<br />
      <input type="password" name="password" /><br />
      <br />
      <input type="submit" value="Zaloguj" />
    </form>
    `);
});

app.post('/login', parser, async (req, res) => {
    try {
        const userId = await promisify(checkUser)(req.body.login, req.body.password);
        req.session.userId = userId;
        res.redirect('/');
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
});

app.get('/register', async (req, res) => {
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    <h1>Test</h1><br />
    <form method="post" action="register">
      Login:<br />
      <input type="text" name="login" /><br />
      Hasło:<br />
      <input type="password" name="password" /><br />
      Powtórz hasło:<br />
      <input type="password" name="passwordRepeat" /><br />
      Imie:<br />
      <input type="text" name="firstName" /><br />
      Nazwisko:<br />
      <input type="text" name="lastName" /><br />
      Data urodzenia:<br />
      <input type="date" name="birthDate" /><br />
      <br />
      <input type="submit" value="Zarejestruj" />
    </form>
    <button onclick="goBack()">Wróc</button>
    `);
});

app.post('/register', parser, async (req, res) => {
    console.log(req.body);
    if(req.body.login === '' || 
    req.body.password === '' || 
    req.body.passwordRepeat === '' || 
    req.body.firstName === '' || 
    req.body.lastName === '') {
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wszytskie pola są obowiązkowe<br>
        <button onclick="goBack()">Wróc</button>
        `);
    } else if(req.body.password != req.body.passwordRepeat) {
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Hasła nie pasują do siebie<br>
        <button onclick="goBack()">Wróc</button>
        `);
    } else {
        var date = null;
        if(req.body.birthDate != null) {
            date = new Date(req.body.birthDate);
        }
        try {
            await promisify(register)(req.body.login, req.body.password, req.body.firstName, req.body.lastName, date);
            res.redirect('/login');
        } catch(err) {
            console.log(err);
            res.send(`
            <script> function goBack() { window.history.back(); } </script>
            Wystąpił błąd<br>
            <button onclick="goBack()">Wróc</button>
            `);
        }
    }
});

app.get('/logout', async (req, res) => {
    req.session.userId = null;
    res.redirect('/');
});

app.get('/getCategories', async (req, res) => {
    const categories = await promisify(getCategories)().catch((err) => console.log(err));
    console.log(categories);
    const mappedCategories = categories.map(cat => "<li>" + cat.name + "</li>").reduce((accumulator, currentValue) => accumulator += currentValue);
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Kategorie produktów:<br /><ul>` +
    mappedCategories +
    `</ul><button onclick="goBack()">Wróc</button>
    `)
});

app.get('/getAllProducts', async (req, res) => {
    const products = await promisify(getAllProducts)().catch((err) => console.log(err));
    console.log(products);
    const mappedProducts = products.map((product, index) => `<tr><td>` + (index + 1) + `</td><td>` + product.name + `</td><td>` + product.price + `</td><td>` + product.category + `</td>` +
    `<td><form action="/addToCart" method="post">
        <input type="hidden" name="productId" value="`+ product._id +`" />
        <button type="submit">Dodaj do koszyka</button>
    </form></td>
    </tr>`).reduce((accumulator, currentValue) => accumulator += currentValue);
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Lista wszystkich produktów:<br /><table border=1 cellpadding="10"><tr><td>Lp</td><td>Nazwa</td><td>Cena</td><td>Kategoria</td><td>
    </td>` +
    mappedProducts +
    `</table><button onclick="goBack()">Wróc</button>
    `)
});

app.get('/getCategory', async (req, res) => {
    try {
    const products = await promisify(getProductsForCatgory)(req.query.cat);
    console.log(products);
    const mappedProducts = products.map((product, index) => "<tr><td>" + index + "</td><td>" + product.name + "</td><td>" + product.price + "</td></tr>").reduce((accumulator, currentValue) => accumulator += currentValue);
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Lista produktów w kategori:<br /><table border=1><tr><td>Lp</td><td>Nazwa</td><td>Cena</td>` +
    mappedProducts +
    `</table><button onclick="goBack()">Wróc</button>
    `)
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
});

app.post('/addToCart', parser, async (req, res) => {
    try {
        const userId = getUserId(req, res)
        await promisify(addToCart)(req.body.productId, userId);
        res.redirect('/cart');
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
})

app.get('/cart', async (req, res) => {
    const userId = getUserId(req, res)
    const products = await promisify(getCartProducts)(userId).catch((err) => console.log(err));
    console.log(products);
    const mappedProducts = products.map((product, index) => `<tr><td>` + (index + 1) + `</td><td>` + product.name + `</td><td>` + product.price + ` zł</td>` +
    `<td><form action="/removeFromCart" method="post">
        <input type="hidden" name="productId" value="`+ product._id +`" />
        <button type="submit">Usuń</button>
    </form></td>
    </tr>`).reduce((accumulator, currentValue) => accumulator += currentValue, "");
    const sum = products.map(product => parseFloat(product.price)).reduce((accumulator, currentValue) => accumulator += currentValue, 0);
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Koszyk:<br /><table border=1><tr><td>Lp</td><td>Nazwa</td><td>Cena</td><td>
    </td>` +
    mappedProducts +
    `</table><br />` +
    sum +
    ` zł<br />
    <a href="/checkout">Zamów<a/>
    <button onclick="goBack()">Wróc</button>
    `)
});

app.post('/removeFromCart', parser, async (req, res) => {
    try {
        await promisify(removeFromCart)(req.body.productId, req.session.userId);
        res.redirect('/cart');
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
})

app.get('/checkout', async (req, res) => {
    const userId = getUserId(req, res)
    try {
        await promisify(checkout)(userId);
        res.redirect('/checkoutHistory');
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
})

app.get('/checkoutHistory', async (req, res) => {
    const userId = getUserId(req, res)
    try {
        const history = await promisify(checkoutHistory)(userId);
        console.log(history);
        const mappedOrders = history.map((order, index) => `<tr><td>` + (index + 1) + `</td><td>` + formatDate(order.orderTime) + `</td><td>` + order.sum + ` zł</td><td>` +
        order.products.map(product => product.name + " " + product.price + " zł<br />").reduce((accumulator, currentValue) => accumulator += currentValue, "")
        + `</td></tr>`)
        .reduce((accumulator, currentValue) => accumulator += currentValue, "");
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Koszyk:<br /><table border=1 cellpadding="10"><tr><td>Lp</td><td>Nazwa</td><td>Cena</td><td>Produkty</td>` +
        mappedOrders +
        `</table><br /><button onclick="goBack()">Wróc</button>
        `)
    } catch(err) {
        console.log(err);
        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wystąpił błąd<br>
        <button onclick="goBack()">Wróc</button>
        `);
    }
})

async function register(login, password, firstName, lastName, birthDate, callback) {
    try {
        await db.collection('users').insertOne({
            firstName: firstName,
            lastName: lastName,
            login: login,
            password: password,
            birthDate: birthDate,
            cart: [],});
        callback();
    } catch(err) {
        callback(err)
    }
}

function getUserId(req, res) {
    if(req.session.userId) {
        return req.session.userId;
    } else {
        res.redirect("/login");
    }
} 

async function getCategories(callback) {
    try {
        db.collection('categories').find().toArray((err, result) => {
            if (err) return console.log(err);
            callback(null, result);
        });
    } catch(err) {
        callback(err, null);
    }
}

async function getAllProducts(callback) {
    try {
        var output = [];
        const products = await db.collection('products').find().toArray();
        console.log(products);
        for(var i = 0; i < products.length; i++) {
            const category = await db.collection('categories').findOne({_id: products[i].category});
            output.push({
                _id: products[i]._id,
                name: products[i].name,
                price: products[i].price,
                category: category.name 
            })
        }
        callback(null, output);
    } catch(err) {
        callback(err, null);
    }
}

async function getProductsForCatgory(category, callback) {
    try {
        var query = { category: ObjectId(category) };
        console.log(category);
        db.collection('products').find(query).toArray((err, result) => {
            if (err) return console.log(err);
            callback(null, result);
        });
    } catch(err) {
        callback(err, null);
    }
}

async function checkUser(login, password, callback) {
    try {
        const result = await db.collection('users').findOne({ login: login });
        if(result.password == password)
            callback(null, result._id);
        else
            callback(err, null)
    } catch(err) {
        callback(err, null);
    }
}

async function getUserData(userId, callback) {
    try {
        const result = await db.collection('users').findOne({ _id: ObjectId(userId) }, {projection: {password: 0}})
        callback(null, result);
    } catch(err) {
        callback(err, null);
    }
}

async function addToCart(productId, userId, callback) {
    try {
        const result = await db.collection('users').updateOne({ _id: ObjectId(userId) }, {$push: { "cart": ObjectId(productId) }});
        callback(null, result);
    } catch(err) {
        callback(err, null);
    }
}

async function getCartProducts(userId, callback) {
    try {
        var userData = await db.collection('users').findOne({ _id: ObjectId(userId) }, {projection: {cart: 1}});
        var productsData = [];
        for(var i = 0; i < userData.cart.length; i++) 
            productsData.push(await db.collection('products').findOne(userData.cart[i]));
        callback(null, productsData);
    } catch(err) {
        callback(err, null);
    }
}

async function checkout(userId, callback) {
    try {
        const currentCart = await promisify(getCartProducts)(userId);
        const sum = currentCart.map(product => parseFloat(product.price)).reduce((accumulator, currentValue) => accumulator += currentValue, 0);
        await db.collection('orders').insertOne({
            userId: ObjectId(userId),
            orderTime: new Date(),
            products: currentCart,
            sum: sum
        });
        await db.collection('users').updateOne({ _id: ObjectId(userId) }, {"$set": {"cart": []} });
        callback(null);
    } catch(err) {
        callback(err, null);
    }
}

async function checkoutHistory(userId, callback) {
    try {
        var orders = await db.collection('orders').find({ userId: ObjectId(userId) }).toArray();
        callback(null, orders);
    } catch(err) {
        callback(err, null);
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

async function seed(callback) {
    try {
        const users = [
            {_id: ObjectId(), firstName: "Adam", lastName: "Kowalski", login: "akowlaski", password: "akowalski", birthDate: new Date('1990-04-11'), cart: []},
            {_id: ObjectId(), firstName: "Michał", lastName: "Nowak", login: "mnowak", password: "mnowak", birthDate: new Date('1980-05-10'), cart: []},
            {_id: ObjectId(), firstName: "Anna", lastName: "Wójcik", login: "awojcik", password: "awojcik", birthDate: new Date('1969-08-30'), cart: []},
            {_id: ObjectId(), firstName: "Jan", lastName: "Lewandowski", login: "jlewandowski", password: "jlewandowski", birthDate: new Date('1975-12-25'), cart: []},
            {_id: ObjectId(), firstName: "Maria", lastName: "Woźniak", login: "mwozniak", password: "mwozniak", birthDate: new Date('1983-01-04'), cart: []},
            {_id: ObjectId(), firstName: "Barbara", lastName: "Wojciechowska", login: "bwojciechowska", password: "bwojciechowska", birthDate: new Date('1992-03-17'), cart: []},
            {_id: ObjectId(), firstName: "Piotr", lastName: "Mazur", login: "pmazur", password: "pmazur", birthDate: new Date('2000-11-31'), cart: []},
            {_id: ObjectId(), firstName: "Janusz", lastName: "Krawczyk", login: "jkrawczyk", password: "jkrawczyk", birthDate: new Date('1978-02-29'), cart: []},
            {_id: ObjectId(), firstName: "Marek", lastName: "Wróbel", login: "mwrobel", password: "mwrobel", birthDate: new Date('1997-07-10'), cart: []},
        ]
        if((await db.collection('users').find().toArray()).length == 0) {
            db.collection('users').insertMany(users);
        }

        const categories = [
            {_id: ObjectId(), name: "Półbuty"},
            {_id: ObjectId(), name: "Kozaki"},
            {_id: ObjectId(), name: "Sandały"},
            {_id: ObjectId(), name: "Sneakersy"},
            {_id: ObjectId(), name: "Mokasyny"},
            {_id: ObjectId(), name: "Baleriny"}
        ]
        if((await db.collection('categories').find().toArray()).length == 0) {
            db.collection('categories').insertMany(categories);
        }

        const products = [
            {_id: ObjectId(), name: "Buty1", category: categories[0]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty2", category: categories[1]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty3", category: categories[2]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty4", category: categories[3]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty5", category: categories[4]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty6", category: categories[5]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty7", category: categories[0]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty8", category: categories[1]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty9", category: categories[2]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty10", category: categories[3]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty11", category: categories[4]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty12", category: categories[5]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty13", category: categories[0]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty14", category: categories[1]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty15", category: categories[2]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty16", category: categories[3]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty17", category: categories[4]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty18", category: categories[5]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty19", category: categories[0]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty20", category: categories[1]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty21", category: categories[2]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty22", category: categories[3]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty23", category: categories[4]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty24", category: categories[5]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
            {_id: ObjectId(), name: "Buty25", category: categories[0]._id, price: (Math.floor(Math.random() * 100000) + 100) / (100)},
        ]
        if((await db.collection('products').find().toArray()).length == 0) {
            db.collection('products').insertMany(products);
        }

        const orders = [
            {_id: ObjectId(), userId: users[0]._id, orderTime: new Date(), products: [products[0], products[1]], sum: (products[0].price + products[1].price)},
            {_id: ObjectId(), userId: users[1]._id, orderTime: new Date(), products: [products[3], products[0]], sum: (products[3].price + products[0].price)},
            {_id: ObjectId(), userId: users[2]._id, orderTime: new Date(), products: [products[6], products[9]], sum: (products[6].price + products[9].price)},
            {_id: ObjectId(), userId: users[3]._id, orderTime: new Date(), products: [products[9], products[5]], sum: (products[9].price + products[5].price)},
            {_id: ObjectId(), userId: users[4]._id, orderTime: new Date(), products: [products[12], products[1]], sum: (products[12].price + products[1].price)},
            {_id: ObjectId(), userId: users[5]._id, orderTime: new Date(), products: [products[15], products[19]], sum: (products[15].price + products[19].price)},
            {_id: ObjectId(), userId: users[6]._id, orderTime: new Date(), products: [products[18], products[23]], sum: (products[18].price + products[23].price)},
            {_id: ObjectId(), userId: users[7]._id, orderTime: new Date(), products: [products[21], products[5]], sum: (products[21].price + products[5].price)},
            {_id: ObjectId(), userId: users[8]._id, orderTime: new Date(), products: [products[21], products[3]], sum: (products[21].price + products[3].price)},
            {_id: ObjectId(), userId: users[0]._id, orderTime: new Date(), products: [products[20], products[9]], sum: (products[20].price + products[9].price)},
            {_id: ObjectId(), userId: users[1]._id, orderTime: new Date(), products: [products[15], products[14]], sum: (products[15].price + products[14].price)},
            {_id: ObjectId(), userId: users[2]._id, orderTime: new Date(), products: [products[17], products[18]], sum: (products[17].price + products[18].price)},
            {_id: ObjectId(), userId: users[3]._id, orderTime: new Date(), products: [products[7], products[21]], sum: (products[7].price + products[21].price)},
            {_id: ObjectId(), userId: users[4]._id, orderTime: new Date(), products: [products[22], products[20]], sum: (products[22].price + products[20].price)},
            {_id: ObjectId(), userId: users[5]._id, orderTime: new Date(), products: [products[4], products[5]], sum: (products[4].price + products[5].price)},
            {_id: ObjectId(), userId: users[6]._id, orderTime: new Date(), products: [products[8], products[10]], sum: (products[8].price + products[10].price)},
            {_id: ObjectId(), userId: users[7]._id, orderTime: new Date(), products: [products[24], products[12]], sum: (products[24].price + products[12].price)},
            {_id: ObjectId(), userId: users[8]._id, orderTime: new Date(), products: [products[11], products[3]], sum: (products[11].price + products[3].price)},
            {_id: ObjectId(), userId: users[0]._id, orderTime: new Date(), products: [products[8], products[4]], sum: (products[8].price + products[4].price)},
            {_id: ObjectId(), userId: users[1]._id, orderTime: new Date(), products: [products[7], products[6]], sum: (products[7].price + products[6].price)},
        ]
        if((await db.collection('orders').find().toArray()).length == 0) {
            db.collection('orders').insertMany(orders);
        }

        callback(null);
    } catch(err) {
        callback(err);
    }
}

app.listen(3000, function () {
    console.log('Library app listening on port 3000!');
  });